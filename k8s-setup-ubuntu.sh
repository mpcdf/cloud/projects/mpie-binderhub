#!/bin/bash

apt-get update
apt-get upgrade -y
apt-get install -y apt-transport-https ca-certificates gnupg2 git

curl https://download.docker.com/linux/ubuntu/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt-get install -y containerd.io docker-ce docker-ce-cli
apt-mark hold containerd.io docker-ce docker-ce-cli

cat > /etc/docker/daemon.json <<EOF
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF

curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://apt.kubernetes.io/ kubernetes-xenial main"

apt-get install -y kubelet=1.18.9-00 kubeadm=1.18.9-00 kubectl=1.18.9-00
apt-mark hold kubelet kubeadm kubectl

cat > /etc/sysctl.d/k8s.conf <<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

echo "Please reboot for changes to take effect!"
