# Install BinderHub on Kubernetes (on OpenStack)

## Procedure

0. Create a private network
    ```sh
    openstack network create k8s-net
    openstack subnet create k8s-subnet --network k8s-net --subnet-range 192.168.0.0/24 --dns-nameserver 130.183.9.32 --dns-nameserver 130.183.1.21
    openstack router create k8s-router
    openstack router set k8s-router --external-gateway cloud-dmz
    openstack router add subnet k8s-router k8s-subnet
    ```

1. Deploy servers
    ```sh
    openstack server create k8s-controller --image "Ubuntu 18.04 LTS" --flavor mpcdf.medium       --key-name $KEYNAME --nic net-id=k8s-net,v4-fixed-ip=192.168.0.10 --user-data k8s-setup-ubuntu.sh
    openstack server create k8s-worker-1   --image "Ubuntu 18.04 LTS" --flavor mpcdf.large.bigmem --key-name $KEYNAME --nic net-id=k8s-net,v4-fixed-ip=192.168.0.11 --user-data k8s-setup-ubuntu.sh
    openstack server create k8s-worker-2   --image "Ubuntu 18.04 LTS" --flavor mpcdf.large.bigmem --key-name $KEYNAME --nic net-id=k8s-net,v4-fixed-ip=192.168.0.12 --user-data k8s-setup-ubuntu.sh
    openstack server create k8s-worker-3   --image "Ubuntu 18.04 LTS" --flavor mpcdf.large.bigmem --key-name $KEYNAME --nic net-id=k8s-net,v4-fixed-ip=192.168.0.13 --user-data k8s-setup-ubuntu.sh
    openstack server create ext-proxy      --image "Ubuntu 18.04 LTS" --flavor mpcdf.small        --key-name $KEYNAME --nic net-id=k8s-net,v4-fixed-ip=192.168.0.100

    openstack floating ip create cloud-dmz
    openstack server add floating ip ext-proxy $FLOATING_IP
    openstack server add security group ext-proxy web
    ```
    Reboot each *kubernetes* node after the setup script has finished, see `/var/log/cloud-init-output.log`.

2. Create the cluster
    ```sh
    # on the controller:
      kubeadm init --config k8s-init.yml
      echo "export KUBECONFIG=/etc/kubernetes/admin.conf" >> .bashrc && source .bashrc
      kubectl apply -f https://docs.projectcalico.org/manifests/calico-vxlan.yaml

    # on the workers:
      kubeadm join 192.168.0.10:6443 --token $TOKEN --discovery-token-ca-cert-hash $HASH
      ```

3. Enable cinder-based persistent volumes
    ```sh
    # on the controller:
      git clone https://github.com/kubernetes/cloud-provider-openstack.git
      rm cloud-provider-openstack/manifests/cinder-csi-plugin/csi-secret-cinderplugin.yaml

      kubectl create secret -n kube-system generic cloud-config --from-file=cloud.conf
      kubectl apply -f cloud-provider-openstack/manifests/cinder-csi-plugin
      kubectl create -f k8s-cinder.yml
    ```

4. Deploy ingress and external load balancer
    ```sh
    # on the controller:
      kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/static/provider/baremetal/deploy.yaml
      kubectl get svc ingress-nginx-controller -n ingress-nginx  # find randomly-assigned node ports for HTTP and HTTPS

    # on the proxy:
      apt update && apt install haproxy
      cat ingress.cfg >> /etc/haproxy/haproxy.cfg  # then edit to make the node ports match those above
      systemctl restart haproxy
    ```

5. Deploy binderhub
    ```sh
    # on the proxy:
      apt install certbot

      systemctl stop haproxy
      certbot certonly --standalone -d binderdemo.mpcdf.mpg.de -d binderdemo-jhub.mpcdf.mpg.de
      systemctl start haproxy

    # on the controller:
      curl https://raw.githubusercontent.com/helm/helm/master/scripts/get-helm-3 | bash
      helm repo add jupyterhub https://jupyterhub.github.io/helm-chart && helm repo update

      kubectl create namespace binder
      kubectl create secret tls binderdemo -n binder --key privkey.pem --cert fullchain.pem
      helm install binder jupyterhub/binderhub --version 0.2.0-n221.h1875bcb --namespace=binder -f binder-config.yml -f binder-secret.yml
    ```

## Notes

* There are multiple paths to expose the binderhub and jupyterhub services to the world, depending whether one uses private vs. public networking, external load balancer vs. ClusterIP+ExternalIP services, and ingress vs. haproxy for TLS termination.
  The particular combination in the procedure was chosen for the following reasons:
    * Private networking gives an extra layer of protection besides the built-in security groups, in that the entire cluster sits behind a NAT router.
      In addition, the cluster can scale without consuming additional public IP addresses.
      It also lets us choose the IP addresses, which are easier to remember.
    * An external load balancer is similar to what you would get on commercial k8s, even though on our current platfrom we have to provision it manually.
      Newer versions of openstack *do* support automated provisioning of load balancers.
      Also, the load balancer with its floating IP is a convenient "jumphost" for admin purposes.
    * While haproxy itself supports TLS termination, it is *not* supported on automatically-provisioned load balancers in openstack.
      Thus, we avoid it to make the transition to the new cloud platform as simple as possible.
      In any case, ingress is very convenient for this purpose and already supported by binderhub and juypterhub.

* Use `-oProxyJump=root@$FLOATING_IP` with `ssh` and `scp` to reach the k8s nodes directly.
  (Newer versions may also support `-J` with the same argument.)

* Calico networking in vxlan mode should behave similarly to flannel.
  Calico networking in the default (non-vxlan) mode will *not* work on openstack unless port security is disabled.
  In principle this is doable on a private network and should give better performance, but has not been tested locally.

* On the current platform, `cloud.conf` must be provided by an openstack admin.
  (Newer versions support user-generated *Application Credentials* for exactly this purpose.)

* Installing the baremetal version of the ingress manifest sets the generic service to type *NodePort*, which is consistent with a manually-provisioned external load balancer.
  One automated load balancer provisioning is available we can revert to the default version.

* Ingress actually does support running services on ports other than 80/443, but we already have two DNS entries (`binderdemo` and `binderdemo-jhub` anyway, plus some remote firewalls may block non-standard ports.
  For development this could be useful, though, since each floating IP comes with only one entry like `vm-130-183-216-xyz.cloud.mpcdf.mpg.de` by default.
  In this case the haproxy configuration and security groups would need further adjustment.

* After step four you should see a nginx-style 404 page when visiting [binderdemo.mpcdf.mpg.de](http://binderdemo.mpcdf.mpg.de).
  This means ingress, haproxy, and the generic ingress service in k8s are all correctly configured.
  Then you don't have to worry about these things in step five!

* The TLS certificate itself can be obtained from letsencrypt or our usual MPG CA.
  It is also possible to auto-renew the letsencrypt certificate from within k8s, but for now this is a manual procedure.

* Config file `binder-secret.yml` has the form
    ```sh
    jupyterhub:
      hub:
        services:
          binder:
            apiToken: $(openssl rand -hex 32)
      proxy:
        secretToken: $(openssl rand -hex 32)

    registry:
      username: $DEPLOY_TOKEN_USERNAME
      password: $DEPLOY_TOKEN_PASSWORD
    ```
    where the *Deploy Token* has scope "read_registry, write_registry" in gitlab.

* The procedure pulls the latest stable version of calico, cinder-csi-plugin, and ingress, which could be a good thing or bad thing...
  Kubernetes itself is locked at 1.18.9 and binderhub at 0.2.0-n221, as these versions are known to work together.
  Binderhub does *not* appear to be compatible with kubernetes 1.19, due to changes to the ingress API.

## Deploy a modified image

0. Create your own image of BinderHub:  
    On a local machine with docker and git, create the modified image of Binderhub that you want to deploy.

    ```
    # Specify properties of the image below
    REFERENCE_REPO=gitlab-registry.mpcdf.mpg.de/mpcdf/cloud/projects/mpie-binderhub
    IMAGE_NAME=binderhub
    IMAGE_VER=1.0.2

    # Cloning the repository for the service images
    git clone git@gitlab.mpcdf.mpg.de:mpcdf/cloud/projects/mpie-binderhub.git
    cd mpie-binderhub

    # Moving into images folder
    pushd images

    # Cloning the original version of BinderHub as starting point
    git clone https://github.com/jupyterhub/binderhub.git
    cd ./binderhub

    # Copying the Dockerfile to the correct location for building and removing original git files 
    rm .dockerignore
    rm -rf .git*
    cp ./helm-chart/images/binderhub/Dockerfile .
    
    ##############################################################################################
    # Make here any modification to the cloned source code                                       #
    # Info available at: https://binderhub.readthedocs.io/en/latest/developer/repoproviders.html #
    ##############################################################################################

    # Build the Docker image
    sudo docker build -t $REFERENCE_REPO/$IMAGE_NAME:$IMAGE_VER .

    # If everything works well, push the source code to git
    # NOTE: this may require the ssh-key of the machine to be
    #       allowed on GIT.
    popd
    git add *
    git commit -m "My changes"
    git push

    # Publish the image to the gitlab repository
    sudo docker login gitlab-registry.mpcdf.mpg.de
    sudo docker push $REFERENCE_REPO/$IMAGE_NAME:$IMAGE_VER
    ```

1. Deploy the modified image:  
    Replace the installation command on the controller
    ```
    helm install binder jupyterhub/binderhub --version 0.2.0-n221.h1875bcb --namespace=binder -f binder-config.yml -f binder-secret.yml
    ```
    with the following commands:
    ```
    # on the controller
    # Retrieving binderhub charts with the info for JupyterHub (from https://jupyterhub.github.io/helm-chart/)
    wget https://jupyterhub.github.io/helm-chart/binderhub-0.2.0-n412.h1f1f0c4.tgz
    tar zxvf binderhub-0.2.0-n412.h1f1f0c4.tgz
    ```
    Modify the binder-config.yml with the following lines to specify the repo location of the modified image that has to be installed
    ```
    image:
      name: $REFERENCE_REPO/$IMAGE_NAME
      tag: $IMAGE_VER
      pullPolicy: Always
    ```
    Install the modified version of BinderHub with
    ```
    helm install binder ./binderhub/ --version 0.2.0-n412.h1f1f0c4 --namespace=binder -f binder-config.yml -f binder-secret.yml
    ```
    Alternatively, update the BinderHub installation with the new image
    ```
    helm upgrade --install binder ./binderhub/ --version 0.2.0-n412.h1f1f0c4 --namespace=binder -f binder-config.yml -f binder-secret.yml
    ```
    
